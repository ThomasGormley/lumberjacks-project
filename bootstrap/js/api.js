import config from '../js/config.js'
const http = async (url, options) => {
  const res = await fetch(url, { ...options });
  console.log(res);
  return res;
};

export async function getTrades() {
  const res = await fetch(config.services.trade.host);
  const json = await res.json();

  return json;
}
