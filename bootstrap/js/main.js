import { getTrades } from "./api.js";

const tradesDataTableBody = document.getElementById("dataTableBody");

const statusCode = {
  0: "Initial State",
  1: "Processing",
  2: "Success",
  3: "Failed",
};
const populateTradesDataTable = async () => {
  const tradesJson = await getTrades();

  await tradesJson.forEach((trade) => {
    console.log(trade);
    const dates = {
      created: new Date(trade.createdTime),
      updated: new Date(trade.updatedTime),
    };
    tradesDataTableBody.insertAdjacentHTML(
      "beforeend",
      `
          <tr>
            <td>${trade.id}</td>
            <td>${trade.stockTicker}</td>
            <td>£${Number(trade.price).toFixed(2)}</td>
            <td>${Number(trade.volume).toFixed(2)}</td>
            <td>£${Number(trade.volume * trade.price).toLocaleString()}</td>
            <td>${statusCode[trade.statusCode]}</td>
            <td>${trade.buyOrSell.toUpperCase()}</td>
            <td>${dates.created.toLocaleDateString()}</td>
            <td>${dates.updated.toLocaleDateString()}</td>
          </tr>
          `
    );
  });
};

populateTradesDataTable();
