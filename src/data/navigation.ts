export default [
  {
    title: 'Dashboard',
    url: '',
  },
  {
    title: 'History',
    url: 'history',
  },
  {
    title: 'Current Shares',
    url: 'currentshares'
  }
];
