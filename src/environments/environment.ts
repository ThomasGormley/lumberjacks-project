// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  services: {
    trade: {
      api: 'http://java-hackathon-lum-java-hackathon-lum.emeadocker13.conygre.com/api/v1/trade',
    },
    price: {
      api: 'https://c4rm9elh30.execute-api.us-east-1.amazonaws.com/default/cachedPriceData?',
    },
    currentshares: {
      api: "http://java-hackathon-lum-java-hackathon-lum.emeadocker13.conygre.com/api/v1/trade/statuscode/2",
    },
    events: {
      api: 'http://events-service-lum-events-service-lum.emeadocker13.conygre.com/api/v1/events',
    },
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
