export const environment = {
  production: true,
  services: {
    trade: {
      api: 'http://java-hackathon-lum-java-hackathon-lum.emeadocker13.conygre.com/api/v1/trade',
    },
    price: {
      api: 'https://c4rm9elh30.execute-api.us-east-1.amazonaws.com/default/cachedPriceData?',
    },
    currentshares: {
      api: "http://java-hackathon-lum-java-hackathon-lum.emeadocker13.conygre.com/api/v1/trade/statuscode/2",
    },
    events: {
      api: 'http://events-service-lum-events-service-lum.emeadocker13.conygre.com/api/v1/events',
    },
  },
};
