import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrentsharesComponent } from './currentshares/currentshares.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HistoryComponent} from './history/history.component';

const routes: Routes = [
  // { path: '', pathMatch: 'full', redirectTo: 'champions' },
  { path: '', component: DashboardComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'currentshares', component: CurrentsharesComponent}

  // { path: 'champions', component: ChampionsListComponent },
  // { path: 'champions/:id', component: ChampionEditComponent },
  // { path: '**', component: ChampionsListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
