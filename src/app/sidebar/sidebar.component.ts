import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import navigationData from '../../data/navigation';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class SidebarComponent implements OnInit {
  navItems: Array<any> = navigationData;
  constructor() {}

  ngOnInit(): void {}
}
