import { Component, OnInit } from '@angular/core';
import { Trades } from '../shared/trade-service/trades';
import { TradesApiService } from '../shared/trade-service/trades-api.service';

@Component({
  selector: 'app-dashboard-stats',
  templateUrl: './dashboard-stats.component.html',
  styleUrls: ['./dashboard-stats.component.css'],
})
export class DashboardStatsComponent implements OnInit {
  trades: any = [];

  statCards: any = [
    {
      title: "Transactions",
      value: "46"
    },
    {
      title: "Transactions",
      value: "46"
    },
    {
      title: "Transactions",
      value: "46"
    }
  ];

  constructor(public tradesService: TradesApiService) {}

  ngOnInit(): void {
    this.fetchTrades();
  }

  fetchTrades() {
    return this.tradesService.getTrades().subscribe(async (data: any) => {
      this.trades = data;
    });
  }

}
