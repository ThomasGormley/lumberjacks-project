import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-stat-card',
  templateUrl: './stat-card.component.html',
  styleUrls: ['./stat-card.component.css'],
})
export class StatCardComponent implements OnInit {
  // @Input() title: string;
  @Input('title') title = '';
  @Input('value') value = '';

  constructor() {}

  ngOnInit(): void {}
}
