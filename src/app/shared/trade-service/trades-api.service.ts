import { Injectable } from '@angular/core';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trades } from './trades';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TradesApiService {
  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  getTrades(): Observable<Trades> {
    return this.http
      .get<Trades>(environment.services.trade.api)
      .pipe(retry(1), catchError(this.handleError));
  }

  getTrade(id: string): Observable<Trades> {
    return this.http
      .get<Trades>(environment.services.trade.api + id)
      .pipe(retry(1), catchError(this.handleError));
  }

  getCurrentShares(): Observable<Trades> {
    return this.http.get<Trades>(environment.services.currentshares.api)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteTrades(id: string): Observable<Trades> {
    return this.http
      .delete<Trades>(environment.services.trade.api + id)
      .pipe(retry(1), catchError(this.handleError));
  }

  createTrades(Trades: Trades): Observable<Trades> {
    return this.http
      .post<Trades>(
        environment.services.trade.api + '',
        JSON.stringify(Trades),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  updateTrade(Trades: Trades): Observable<Trades> {
    console.log({
      Trades,
    });
    return this.http
      .put<Trades>(
        environment.services.trade.api + '',
        JSON.stringify(Trades),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));

    // res.toPromise().then((data) => console.log(data));

    // return res;
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
