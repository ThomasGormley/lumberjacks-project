export class Trades {


    constructor(){
        this.id=0;
        this.stockTicker="";
        this.price=0;
        this.volume=0;
        this.buyOrSell= "";
        this.statusCode = 0;
        this.createdTime="";
        this.updatedTime="";
        
    }
    id: number;
    stockTicker: string;
    price: number;
    volume: number;
    buyOrSell: string;
    statusCode: number;
    createdTime: string;
    updatedTime: string;
   

}
