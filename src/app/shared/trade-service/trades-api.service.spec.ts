import { TestBed } from '@angular/core/testing';

import { TradesApiService } from './trades-api.service';

describe('TradesApiService', () => {
  let service: TradesApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TradesApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
