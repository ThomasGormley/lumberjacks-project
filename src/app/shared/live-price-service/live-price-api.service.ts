import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PriceData } from './price-data';

@Injectable({
  providedIn: 'root',
})
export class LivePriceApiService {
  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  getPriceData(ticker: string): Observable<PriceData> {
    return this.http
      .get<PriceData>(`${environment.services.price.api}ticker=${ticker}`)
      .pipe(retry(1), catchError(this.handleError));
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `There was an error fetching live price data, please try a different stock ticker`;
    }
    console.log(error)
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
