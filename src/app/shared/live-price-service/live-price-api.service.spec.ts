import { TestBed } from '@angular/core/testing';

import { LivePriceApiService } from './live-price-api.service';

describe('LivePriceApiService', () => {
  let service: LivePriceApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LivePriceApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
