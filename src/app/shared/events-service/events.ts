export class Events {



    constructor(){
        this.eventID=0;
        this.tradeID=0;
        this.eventType="";
        this.createdAt="";
        this.payload="";
    }
    eventID: number;
    tradeID: number;
    eventType: string;
    createdAt: string;
    payload: string;

}
