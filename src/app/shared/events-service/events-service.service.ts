import { Injectable } from '@angular/core';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Events } from './events';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class EventsServiceService {
  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  getEvents(): Observable<Events> {
    return this.http
      .get<Events>(environment.services.events.api)
      .pipe(retry(1), catchError(this.handleError));
  }

  getTrade(id: string): Observable<Events> {
    return this.http
      .get<Events>(environment.services.events.api + id)
      .pipe(retry(1), catchError(this.handleError));
  }

  deleteTrades(id: string): Observable<Events> {
    return this.http
      .delete<Events>(environment.services.events.api + id)
      .pipe(retry(1), catchError(this.handleError));
  }

  createTrades(Events: Events): Observable<Events> {
    return this.http
      .post<Events>(
        environment.services.trade.api + '',
        JSON.stringify(Events),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  updateTrade(Trades: Events): Observable<Events> {
    console.log({
      Events,
    });
    return this.http
      .put<Events>(
        environment.services.trade.api + '',
        JSON.stringify(Events),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));

    // res.toPromise().then((data) => console.log(data));

    // return res;
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
