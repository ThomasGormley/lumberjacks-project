import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { LivePriceApiService } from '../shared/live-price-service/live-price-api.service';

@Component({
  selector: 'app-live-stocks',
  templateUrl: './live-stocks.component.html',
  styleUrls: ['./live-stocks.component.css'],
})
export class LiveStocksComponent implements OnInit {
  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: ChartOptions & { annotation?: any } = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];

  ticker: string = "";

  constructor(public priceApi: LivePriceApiService) {
    this.ticker = 'AMZN';
  }

  ngOnInit(): void {
    this.fetchPriceData();
  }

  fetchPriceData() {
    
    return this.priceApi.getPriceData(this.ticker).subscribe((data: any) => {
      // console.log(data);
      // console.log(data.price_data.close);
      // console.log(data.price_data.timestamp);
      this.lineChartLabels = data.price_data.timestamp.slice(Math.max(data.price_data.timestamp.length - 44, 0));
      this.lineChartData = [
        { data: data.price_data.close.slice(Math.max(data.price_data.timestamp.length - 44, 0)), label: this.ticker },
      ];

    });
  }
}
