import { Component, Input, OnInit } from '@angular/core';
import { TradesApiService } from 'src/app/shared/trade-service/trades-api.service';

@Component({
  selector: 'app-sell-card',
  templateUrl: './sell-card.component.html',
  styleUrls: ['./sell-card.component.css'],
})
export class SellCardComponent implements OnInit {
  @Input() sellDetails = {
    buyOrSell: 'SELL',
    price: 0,
    stockTicker: '',
    updatedTime: '',
    volume: 0,
    id: 0,
    statusCode: 0,
    createdTime: '',
  };

  constructor(public tradeApi: TradesApiService) {}

  ngOnInit(): void {}

  addTrade() {
    this.tradeApi.createTrades(this.sellDetails).subscribe((data: {}) => {
      console.log(data);
    });
  }
}
