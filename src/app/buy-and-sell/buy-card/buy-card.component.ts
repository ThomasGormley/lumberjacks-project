import { Component, Input, OnInit } from '@angular/core';
import { TradesApiService } from 'src/app/shared/trade-service/trades-api.service';

@Component({
  selector: 'app-buy-card',
  templateUrl: './buy-card.component.html',
  styleUrls: ['./buy-card.component.css'],
})
export class BuyCardComponent implements OnInit {
  @Input() buyDetails = {
    buyOrSell: 'BUY',
    price: 0,
    stockTicker: '',
    updatedTime: '',
    volume: 0,
    id: 0,
    statusCode: 0,
    createdTime: '',
  };

  constructor(public tradeApi: TradesApiService) {}

  ngOnInit(): void {}

  addTrade() {
    this.tradeApi.createTrades(this.buyDetails).subscribe((data: {}) => {
      console.log(data);
    });
  }
}
