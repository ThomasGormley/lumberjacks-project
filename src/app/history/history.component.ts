import { Component, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import {EventsServiceService} from "../shared/events-service/events-service.service";
import { TradesApiService } from '../shared/trade-service/trades-api.service';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

Events: any = [];

  constructor(
    public eventApi : EventsServiceService,
    public tradeApi : TradesApiService
  ) { }

  ngOnInit(): void {
    this.loadEvents()
  }

  statusCode: any = {
    0: "Initial State",
    1: "Processing",
    2: "Success",
    3: "Failed",
  };

  loadEvents(){
    return this.eventApi.getEvents().subscribe((events: any)=>{
      console.log(events)
      this.tradeApi.getTrades().subscribe((trades: any) => {
        console.log("We got trades!");
        console.log(trades);

        for(let event of events) {
          for(let trade of trades) {
            console.log(this.statusCode[trade.statusCode])
            if(event.tradeID == trade.id) {
              event['stockTicker'] = trade.stockTicker;
              event['statusCode'] = trade.statusCode;
              event['status'] = this.statusCode[trade.statusCode];

              break;
            }
          }

        }


        

        this.Events = events
        
      })
    })
  }
  // {eventID: 1, tradeID: 0, eventType: 'TRADE_BUY', createdAt:
    columnDefs: ColDef[] = [
      { field: 'eventID', sortable: true, filter: true },
      { field: 'tradeID', sortable: true, filter: true },
      { field: 'stockTicker', sortable: true, filter: true },
      { field: 'eventType', sortable: true, filter: true },
      { field: 'createdAt', sortable: true, filter: true },
      { field: 'statusCode',sortable: true, filter: true  },
      { field: 'status',sortable: true, filter: true  },
    ];

  

}

