import { Component, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { TradesApiService } from '../shared/trade-service/trades-api.service';

@Component({
  selector: 'app-currentshares',
  templateUrl: './currentshares.component.html',
  styleUrls: ['./currentshares.component.css']
})
export class CurrentsharesComponent implements OnInit {

  trades: any = [];

  columnDefs: ColDef[] = [
    { field: 'id', sortable: true, filter: true },
    { field: 'stockTicker', sortable: true, filter: true },
    { field: 'createdTime', sortable: true, filter: true },
    { field: 'updatedTime', sortable: true, filter: true },
    { field: 'statusCode', sortable: true, filter: true },
    { field: 'price', sortable: true, filter: true },
    { field: 'volume', sortable: true, filter: true },
  ];

  constructor(public tradesService: TradesApiService) {}

  ngOnInit(): void {
    this.loadCurrentShares()
  }

  loadCurrentShares() {
    return this.tradesService.getCurrentShares().subscribe((data: any)=> {
      // console.log(data)

      this.trades = data;
      this.trades = this.trades.filter((trade: any) =>trade.buyOrSell.toLowerCase() == "buy")
      console.log(this.trades)
    })

  }

}
