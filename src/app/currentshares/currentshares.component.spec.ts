import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentsharesComponent } from './currentshares.component';

describe('CurrentsharesComponent', () => {
  let component: CurrentsharesComponent;
  let fixture: ComponentFixture<CurrentsharesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrentsharesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentsharesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
