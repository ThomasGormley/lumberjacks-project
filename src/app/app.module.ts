import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AgGridModule  } from 'ag-grid-angular';
import { DashboardStatsComponent } from './dashboard-stats/dashboard-stats.component';
import { HistoryComponent } from './history/history.component';
import { StatCardComponent } from './dashboard-stats/stat-card/stat-card.component';
import { HttpClientModule } from '@angular/common/http';
import { CurrentsharesComponent } from './currentshares/currentshares.component';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BuyAndSellComponent } from './buy-and-sell/buy-and-sell.component';
import { BuyCardComponent } from './buy-and-sell/buy-card/buy-card.component';
import { SellCardComponent } from './buy-and-sell/sell-card/sell-card.component';
import { LiveStocksComponent } from './live-stocks/live-stocks.component';


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    DashboardComponent,
    DashboardStatsComponent,
    HistoryComponent,
    StatCardComponent,
    CurrentsharesComponent,
    LiveStocksComponent,
    BuyAndSellComponent,
    BuyCardComponent,
    SellCardComponent
  ],
  imports: [
    BrowserModule,
    AgGridModule.withComponents([]),
    NgbModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
